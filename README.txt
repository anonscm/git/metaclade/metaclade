# Let ${MCLADE_DIR} be the absolute directory
# in which MetaCLADE's source has been extracted
MCLADE_DIR=${PWD}

# Put CLADE models in the two following directories:
#     ${MCLADE_DIR}/data/models/pssms/
#     ${MCLADE_DIR}/data/models/hmms/
# and then extact them with the following commands.
cd ${MCLADE_DIR}/data/models/
tar -xf hmms.clade.27.tar.gz
tar -xf pssms.clade.27.tar.gz

# Extract CCM and SCM 2-dimensional space definitions
cd ${MCLADE_DIR}/data/models/
tar -xf ccm_breaks.tar.gz
tar -xf scm_breaks.tar.gz

# Extract list of model identifiers
cd ${MCLADE_DIR}/data/pfamLists
tar -xf used.tar.gz

# Decompress model dictionary
gzip -d ${MCLADE_DIR}/files/models_dict.txt.gz
