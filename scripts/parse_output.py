"""
Author: Ari Ugarte
"""
import sys
import os
import time
import glob
import argparse
import mcladeScriptFunctions as FUN
from operator  import itemgetter

#SCRIPT CONSTANTS
CCM_TYPE                 = "CCM"
SCM_TYPE                 = "SCM"

OVERLAPPING_THRESHOLD    = .85
MIN_SCORE                = 0.0;
CLASS_POS                = "POS"
CLASS_NEG                = "NEG"
ARFF_HEADER              = "@relation %s\n\n@attribute score numeric\n@attribute mean_score numeric\n@attribute class {%s, %s}\n\n@data\n"

ACC_NUMBER_KEY           = "ACC_NUMBER"
SEQ_ID_KEY               = "SEQ_ID"
EVALUE_KEY               = "EVALUE"
START_KEY                = "START"
END_KEY                  = "END"
SCORE_KEY                = "SCORE"
IDENTITY_KEY             = "IDENTITY"
SPECIE_KEY               = "SPECIE"
MODEL_START_KEY          = "MODEL_START"
MODEL_END_KEY            = "MODEL_END" 

CCM_ACC_NUMBER_COL       = 0
CCM_SEQ_ID_COL           = 3
CCM_EVALUE_COL           = 4
CCM_START_COL            = 5
CCM_END_COL              = 6
CCM_SCORE_COL            = 7
CCM_IDENTITY_COL         = 8
CCM_SPECIE_COL           = 9
CCM_MODEL_START_COL      = 10
CCM_MODEL_END_COL        = 11 

SCM_ACC_NUMBER_COL       = 0
SCM_SEQ_ID_COL           = 3
SCM_EVALUE_COL           = 4
SCM_START_COL            = 5
SCM_END_COL              = 6
SCM_SCORE_COL            = 7
SCM_IDENTITY_COL         = 8
SCM_SPECIE_COL           = 9
SCM_MODEL_START_COL      = 10
SCM_MODEL_END_COL        = 11 


MODEL_SEARCH_RESULT_COLS = {
CCM_TYPE : {ACC_NUMBER_KEY  : CCM_ACC_NUMBER_COL,
            SEQ_ID_KEY      : CCM_SEQ_ID_COL,
            EVALUE_KEY      : CCM_EVALUE_COL,
            START_KEY       : CCM_START_COL,
            END_KEY         : CCM_END_COL,
            SCORE_KEY       : CCM_SCORE_COL,
            IDENTITY_KEY    : CCM_IDENTITY_COL,
            SPECIE_KEY      : CCM_SPECIE_COL,
            MODEL_START_KEY : CCM_MODEL_START_COL,
            MODEL_END_KEY   : CCM_MODEL_END_COL
           },
SCM_TYPE : {ACC_NUMBER_KEY  : SCM_ACC_NUMBER_COL,
            SEQ_ID_KEY      : SCM_SEQ_ID_COL,
            EVALUE_KEY      : SCM_EVALUE_COL,
            START_KEY       : SCM_START_COL,
            END_KEY         : SCM_END_COL,
            SCORE_KEY       : SCM_SCORE_COL,
            IDENTITY_KEY    : SCM_IDENTITY_COL,
            SPECIE_KEY      : SCM_SPECIE_COL,
            MODEL_START_KEY : SCM_MODEL_START_COL,
            MODEL_END_KEY   : SCM_MODEL_END_COL
           }
}

#ARGUMENTS
SEARCH_OUTPUT_DIR_NAME       = "search_output_dir_name"
ACC_ID                       = "acc_id"
ARFF_OUT_DIR                 = "arff_out_dir"
CCM_SEARCH_RES_EXT           = "ccm_search_res_ext"
SCM_SEARCH_RES_EXT           = "scm_search_res_ext"
CCM_ARFF_RES_EXT             = "ccm_arff_res_ext"
SCM_ARFF_RES_EXT             = "scm_arff_res_ext"
CCM_RESULT_RESUME            = "ccm_result_resume"
SCM_RESULT_RESUME            = "scm result resume"

#MESSAGES
DESCRIPTION                  = "This script filters MetaCLADE overlapping hits for a domain and creates\n a reduced file and an arff file with filtered results"
SEARCH_OUTPUT_DIR_NAME_HELP  = "Model search output directory"
ACC_ID_HELP                  = "Accesion of the domain"
ARFF_OUT_DIR_HELP            = "Directory for arff files"
CCM_SEARCH_RES_EXT_HELP      = "CCM model output extention"
SCM_SEARCH_RES_EXT_HELP      = "SCM model output extention"
CCM_ARFF_RES_EXT_HELP        = "CCM arff file extention"
SCM_ARFF_RES_EXT_HELP        = "SCM arff file extention"
CCM_RESULT_RESUME_HELP       = "CCM overlapping filter file"
SCM_RESULT_RESUME_HELP       = "SCM overlapping filter file"

def writeArffAndBestDomains(resDictBestDoms,resDict,arff_out_path,bestdomains_out_path,acc_id):
    with open(arff_out_path,"w") as arff_out_file,  open(bestdomains_out_path,"w") as bestdomains_out_file:
        arff_out_file.write(ARFF_HEADER % (acc_id,CLASS_POS,CLASS_NEG))
        for seq in resDictBestDoms:
            for key in resDictBestDoms[seq]:
                accNumber,evalue,start,end,score,identity,specie,model_start,model_end = resDict[seq][key]
                try :
                    meanScore     = float(score) / (float(end) - float(start))
                    arff_out_file.write("%f,%f,?\n" % (score,meanScore))
                    bestdomains_out_file.write("%s\t%d\t%d\t%s\t%s\t%d\t%d\t%.2f\t%.2f\t%s\n" %(accNumber,model_start,model_end,seq,evalue,start,end,score,identity,specie))
                except ZeroDivisionError:
                    pass

def getBestDomains(lines,type):
    resDict         = {}
    resDictBestDoms = {}
    for elts in lines:
        accNumber,seqId,evalue,start,end,score,identity,specie,model_start,model_end = getColumnValues(elts,type)
        key = "%d_%d_%s" % (start,end,accNumber)
        if score < MIN_SCORE:
            break;
        try:#Checks if this read has already some matches in the dictionary
            resDict[seqId]
            try:
                resDict[seqId][key]
            except KeyError: #If the exact positions of the region has not been examinated
                is_new_BD = True
                for k in resDictBestDoms[seqId]:
                    _,_,bdstart,bdend,_,_,_,_,_ = resDict[seqId][k]
                    overlapping                 = FUN.calculateOverlapSize(start,end,bdstart,bdend)
                    overlappingAA               = overlapping[FUN.OVERLAP_AA]
                    overlappingPerc             = overlapping[FUN.PERC_OVERLAP]
                    overlappingPerc_r           = overlapping[FUN.PERC_OVERLAP_R]
                    if overlappingPerc >= OVERLAPPING_THRESHOLD and overlappingPerc_r >= OVERLAPPING_THRESHOLD: #Determine if the region is a new best domain
                        is_new_BD = False
                if is_new_BD: # If the region is a new best domain
                    resDictBestDoms[seqId].append(key)
                resDict[seqId][key]    = (accNumber,evalue,start,end,score,identity,specie,model_start,model_end)
        except KeyError:
            resDict[seqId]         = {key : (accNumber,evalue,start,end,score,identity,specie,model_start,model_end)}
            resDictBestDoms[seqId] = [key]
    return (resDictBestDoms,resDict)

def filterResults(lines,type,outArff,outBD,acc_id):
    lines.sort(key=lambda x:float(itemgetter(MODEL_SEARCH_RESULT_COLS[type][SCORE_KEY])(x)),reverse=True)
    resDictBestDoms,resDict = getBestDomains(lines,type)
    writeArffAndBestDomains(resDictBestDoms,resDict,outArff,outBD,acc_id)

def getColumnValues(columns,type):    
    return (columns[MODEL_SEARCH_RESULT_COLS[type][ACC_NUMBER_KEY]],
            columns[MODEL_SEARCH_RESULT_COLS[type][SEQ_ID_KEY]],
            columns[MODEL_SEARCH_RESULT_COLS[type][EVALUE_KEY]],
            int(columns[MODEL_SEARCH_RESULT_COLS[type][START_KEY]]),
            int(columns[MODEL_SEARCH_RESULT_COLS[type][END_KEY]]),
            float(columns[MODEL_SEARCH_RESULT_COLS[type][SCORE_KEY]]),
            float(columns[MODEL_SEARCH_RESULT_COLS[type][IDENTITY_KEY]]),
            columns[MODEL_SEARCH_RESULT_COLS[type][SPECIE_KEY]],
            int(columns[MODEL_SEARCH_RESULT_COLS[type][MODEL_START_KEY]]),
            int(columns[MODEL_SEARCH_RESULT_COLS[type][MODEL_END_KEY]]) )

def main():
    start_time = time.time()
    parser     = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument(SEARCH_OUTPUT_DIR_NAME,  help=SEARCH_OUTPUT_DIR_NAME_HELP, type=str)
    parser.add_argument(ACC_ID, help=ACC_ID_HELP, type=str)
    parser.add_argument(ARFF_OUT_DIR, help=ARFF_OUT_DIR_HELP, type=str)
    parser.add_argument(CCM_SEARCH_RES_EXT, help=CCM_SEARCH_RES_EXT_HELP, type=str)
    parser.add_argument(SCM_SEARCH_RES_EXT, help=SCM_SEARCH_RES_EXT_HELP, type=str)
    parser.add_argument(CCM_ARFF_RES_EXT, help=CCM_ARFF_RES_EXT_HELP, type=str)
    parser.add_argument(SCM_ARFF_RES_EXT, help=SCM_ARFF_RES_EXT_HELP, type=str)
    parser.add_argument(CCM_RESULT_RESUME, help=CCM_RESULT_RESUME_HELP, type=str)
    parser.add_argument(SCM_RESULT_RESUME, help=SCM_RESULT_RESUME_HELP, type=str)
    args       = vars(parser.parse_args())
    dir        = "%s%s" % (args[SEARCH_OUTPUT_DIR_NAME],args[ACC_ID])
    if not os.path.exists(args[ARFF_OUT_DIR]):
        os.makedirs(args[ARFF_OUT_DIR])
    if os.path.exists(dir):
        lines = []
        for path in glob.iglob("%s/*%s" %(dir,args[CCM_SEARCH_RES_EXT])):
            with open(path) as file:
                lines += [line.split() for line in file]
        arff_out_path          = "%s%s%s" % (args[ARFF_OUT_DIR], args[ACC_ID], args[CCM_ARFF_RES_EXT])
        bestdomains_out_path   = "%s/%s"  % (dir, args[CCM_RESULT_RESUME])
        filterResults(lines, CCM_TYPE, arff_out_path, bestdomains_out_path, args[ACC_ID])
        try:
            with open("%s/%s%s" % (dir,args[ACC_ID],args[SCM_SEARCH_RES_EXT])) as file:
                lines = [line.split() for line in file]
            arff_out_path        = "%s%s%s" % (args[ARFF_OUT_DIR], args[ACC_ID], args[SCM_ARFF_RES_EXT])
            bestdomains_out_path = "%s/%s"  % (dir, args[SCM_RESULT_RESUME])
            filterResults(lines, SCM_TYPE, arff_out_path, bestdomains_out_path, args[ACC_ID])
        except IOError:
            pass
    print("%s\t%f\n"%(args[ACC_ID],time.time() - start_time));
    return 1

if __name__ == "__main__":
    sys.exit(main())
