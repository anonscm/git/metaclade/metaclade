"""
Author: Ari Ugarte
"""
import sys
import os
import time
import argparse
import cPickle

#SCRIPT CONSTANTS
#MINIMAL DEFAULT IF SOMETHING GOES WRONG
MINIMAL_DEF_CCM             = 50
MINIMAL_DEF_SCM             = 25
CLASS_POS                   = "POS"

#ARGUMENTS
ACC_LIST                    = "acc_list"
EVAL_OUT_DIR                = "eval_out_dir"
CCM_EVAL_RES_EXT            = "ccm_eval_res_ext"
SCM_EVAL_RES_EXT            = "scm_eval_res_ext"
SEARCH_OUTPUT_DIR_NAME      = "search_output_dirname"
CCM_RESULT_RESUME           = "ccm_result_resume"
SCM_RESULT_RESUME           = "scm_result_resume"
BD_RESULT_DIR_NAME          = "bestdomains_result_dir_name"
NB_FILTER_OUT_FILE          = "nb_filter_out_file"
NB_THRESHOLD                = "nb_threshold"
MIN_NEG_CCM                 = "min_neg_ccm"
MIN_NEG_SCM                 = "min_neg_scm"
MIN_POS_CCM                 = "min_pos_ccm"
MIN_POS_SCM                 = "min_pos_scm"

#MESSAGES
DESCRIPTION                 = "This scripts creates the best domains file (NB filter)"
ACC_LIST_HELP               = "List of domain accesion numbers"
EVAL_OUT_DIR_HELP           = "Directory for probabilities results"
CCM_EVAL_RES_EXT_HELP       = "CCM probabilities results extention"
SCM_EVAL_RES_EXT_HELP       = "SCM probabilities results extention"
SEARCH_OUTPUT_DIR_NAME_HELP = "Model search output directory"
CCM_RESULT_RESUME_HELP      = "CCM overlapping filter file"
SCM_RESULT_RESUME_HELP      = "SCM overlapping filter file"
BD_RESULT_DIR_NAME_HELP     = "Directory for best domains and final predictions"
NB_FILTER_OUT_FILE_HELP     = "Best domains file name"
NB_THRESHOLD_HELP           = "Naives Bayes Classifier Threshold [0-1]"
MIN_NEG_CCM_HELP            = "CCM Minimal Scores for Negatives"
MIN_NEG_SCM_HELP            = "SCM Minimal Scores for Negatives"
MIN_POS_CCM_HELP            = "CCM Minimal Scores for Positives"
MIN_POS_SCM_HELP            = "SCM Minimal Scores for Positives"

def getDomainsIndexes(path,threshold):
    lineNumbers = {}
    if (os.path.exists(path)):
        with open(path) as pathFile:
            for i in range(0,5):
                line = pathFile.readline();
            while 1:
                line = pathFile.readline().strip()
                elts = line.split()
                if not line:
                    break;
                if len(elts) == 4:
                    if CLASS_POS in elts[2] and float(elts[3]) >= threshold:
                        lineNumbers[elts[0]] = (float(elts[3]),elts[2])
    return lineNumbers

def writeDomains(outFile,lineNumbers,bestPath,minimalScore,pfam):
    countBD  = 0 
    if len(lineNumbers) > 0 and os.path.exists(bestPath):
        for line in open(bestPath):
            countBD+=1
            try:
                prob,label = lineNumbers[str(countBD)]
                scoreft = float(line.strip().split()[7])
                if scoreft > minimalScore:
                    outFile.write("%s\t%s\t%.2f\t%s\n" % (line.strip(),pfam,prob,label))
            except KeyError:
                pass

def getMinimalScore(pfam,minimalNegDict,minimalPosDict,minimalDef):
    try:
        minimalScore = float(minimalNegDict[pfam])
    except KeyError:
        minimalScore = minimalDef #if something is wrong (missing values) take default threshold
    try:
        minimalPosScore = float(minimalPosDict[pfam])
    except KeyError:
        minimalPosScore = minimalDef   #if something is wrong (missing values) take default threshold
    if minimalPosScore > minimalScore: #if the less significant poisitive has greater score, set it as low boundarie
        minimalScore = minimalPosScore
    return minimalScore

def main():
    start_time = time.time()
    parser     = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument(ACC_LIST, help=ACC_LIST_HELP, type=str)
    parser.add_argument(EVAL_OUT_DIR, help=EVAL_OUT_DIR_HELP, type=str)
    parser.add_argument(CCM_EVAL_RES_EXT, help=CCM_EVAL_RES_EXT_HELP, type=str)
    parser.add_argument(SCM_EVAL_RES_EXT, help=SCM_EVAL_RES_EXT_HELP, type=str)
    parser.add_argument(SEARCH_OUTPUT_DIR_NAME,  help=SEARCH_OUTPUT_DIR_NAME_HELP, type=str)
    parser.add_argument(CCM_RESULT_RESUME, help=CCM_RESULT_RESUME_HELP, type=str)
    parser.add_argument(SCM_RESULT_RESUME, help=SCM_RESULT_RESUME_HELP, type=str)
    parser.add_argument(BD_RESULT_DIR_NAME, help=BD_RESULT_DIR_NAME_HELP, type=str)
    parser.add_argument(NB_FILTER_OUT_FILE, help=NB_FILTER_OUT_FILE_HELP, type=str)
    parser.add_argument(NB_THRESHOLD, help=NB_THRESHOLD_HELP, type=float)
    parser.add_argument(MIN_NEG_CCM, help=MIN_NEG_CCM_HELP, type=str)
    parser.add_argument(MIN_NEG_SCM, help=MIN_NEG_SCM_HELP, type=str)
    parser.add_argument(MIN_POS_CCM, help=MIN_POS_CCM_HELP, type=str)
    parser.add_argument(MIN_POS_SCM, help=MIN_POS_SCM_HELP, type=str)
    args       = vars(parser.parse_args())
    if not os.path.exists(args[BD_RESULT_DIR_NAME]):
        os.makedirs(args[BD_RESULT_DIR_NAME])
    with open(args[MIN_NEG_CCM], "r") as file:
        minimal_CCM_neg_dict = cPickle.load(file)
    with open(args[MIN_NEG_SCM], "r") as file:
        minimal_SCM_neg_dict = cPickle.load(file)
    with open(args[MIN_POS_CCM], "r") as file:
        minimal_CCM_pos_dict = cPickle.load(file)
    with open(args[MIN_POS_SCM], "r") as file:
        minimal_SCM_pos_dict = cPickle.load(file)
    with open("%s%s" % (args[BD_RESULT_DIR_NAME],args[NB_FILTER_OUT_FILE]),"w") as outFile:
        for line in open(args[ACC_LIST]):
            pfam     = line.split()[0]
            #CCM
            path     = "%s%s%s"  % (args[EVAL_OUT_DIR], pfam, args[CCM_EVAL_RES_EXT])
            bestPath = "%s%s/%s" % (args[SEARCH_OUTPUT_DIR_NAME], pfam, args[CCM_RESULT_RESUME])
            line_numbers      = getDomainsIndexes(path,args[NB_THRESHOLD])
            minimal_score_ccm = getMinimalScore(pfam,minimal_CCM_neg_dict,minimal_CCM_pos_dict,MINIMAL_DEF_CCM)
            writeDomains(outFile,line_numbers,bestPath,minimal_score_ccm,pfam)
            #SCM
            path     = "%s%s%s"  % (args[EVAL_OUT_DIR], pfam, args[SCM_EVAL_RES_EXT])
            bestPath = "%s%s/%s" % (args[SEARCH_OUTPUT_DIR_NAME], pfam, args[SCM_RESULT_RESUME])
            line_numbers      = getDomainsIndexes(path,args[NB_THRESHOLD])
            minimal_score_scm = getMinimalScore(pfam,minimal_SCM_neg_dict,minimal_SCM_pos_dict,MINIMAL_DEF_SCM)
            writeDomains(outFile,line_numbers,bestPath,minimal_score_scm,pfam)
    print("%s\t%f\n"%("Total time",time.time() - start_time));
    return 1

if __name__ == "__main__":
    sys.exit(main())