"""
Author: Ari Ugarte
"""

import sys
import os
import argparse
import time
import operator
import mcladeScriptFunctions as FUN

#SCRIPT CONSTANTS
I_EVALUE                       = "i_evalue"
MODEL_NAME                     = "model_name"
TARGET_ACC                     = "target_acc"
NB_PROB                        = "nb_prob"
MODEL_START                    = "model_start"
MODEL_END                      = "model_end"
SEQ_START                      = "seq_start"
SEQ_END                        = "seq_end"
SCORE                          = "score"
IDENTITY                       = "identity"
TAXON                          = "taxon"
SORT_SCORE                     = "sort_score"
QUERY_NAME                     = "query_name"
HMMER_MODEL                    = "HMMer-3"
FP_OVERLAPPING_THRESHOLD       = 10

#ARGUMENTS
BD_RESULT_DIR_NAME             = "bestdomains_result_dir_name"
NB_FILTER_OUT_FILE             = "nb_filter_out_file"
ALL_OL_FILTER_OUT_DIR          = "all_ol_filter_out_dir"
FINAL_PREDICTION_OUT_FILE      = "final_prediction_out_file"

DESCRIPTION                    = "This script generates final non-overlapping mclade predictions"
BD_RESULT_DIR_NAME_HELP        = "Directory for best domains and final predictions"
NB_FILTER_OUT_FILE_HELP        = "Best domains file name"
ALL_OL_FILTER_OUT_DIR_HELP     = "Directory for best domains and final predictions"
FINAL_PREDICTION_OUT_FILE_HELP = "Final Prediction file name"

#MESSAGES

def getFactor(score):
	if score >= 110:
		return 1
	elif score >= 90:
		return 1
	elif score>=70:
		return .8
	elif score>=60:
		return .7
	elif score>=50:
		return .6
	elif score>=40:
		return .5
	else:
		return .4

def getFactorHMM(score):
	if score >= 40:
		return 1
	elif score >= 35:
		return .9
	elif score >= 30:
		return .8
	elif score >= 25:
		return .7
	elif score >= 20:
		return .5
	elif score >= 15:
		return .4
	else:
		return .3

def filterOverlappingPredictions(bestDomainsPath,finalPredictionsPath):
	objReturn = {}
	rankobj   = {}
	with open(finalPredictionsPath, "w") as outFile:	
		for line in open(bestDomainsPath):
			elts    = line.strip().split()
			data    = {}
			data[MODEL_NAME]   = elts[0]
			data[TARGET_ACC]   = elts[10]
			data[NB_PROB]      = float(elts[11])
			data[I_EVALUE]     = float(elts[4])
			data[MODEL_START]  = int(elts[1])
			data[MODEL_END]    = int(elts[2])
			data[SEQ_START]    = int(elts[5])
			data[SEQ_END]      = int(elts[6])
			data[SCORE]        = float(elts[7])
			data[IDENTITY]     = float(elts[8])
			data[TAXON]        = float(elts[8])
			if data[MODEL_NAME] == HMMER_MODEL:
				data[SORT_SCORE] = getFactorHMM(data[SCORE])
			else:
				data[SORT_SCORE] = getFactor(data[SCORE]) * float(data[IDENTITY]/100.0)
			data[QUERY_NAME]   = elts[3]
			try:
				rankobj[data[QUERY_NAME]].append(data)
			except KeyError:
				rankobj[data[QUERY_NAME]] = [data]
		for query in rankobj:
			objReturn[query] = []
			accHMMer         = set()
			sortIndexRes     = {}
			for i in range(0,len(rankobj[query])):
				imodel  = rankobj[query][i][MODEL_NAME]
				tacc    = rankobj[query][i][TARGET_ACC] 
				if imodel == HMMER_MODEL:
					accHMMer.add(tacc)
			for i in range(0,len(rankobj[query])):
				imodel = rankobj[query][i][MODEL_NAME]
				tacc   = rankobj[query][i][TARGET_ACC]
				if imodel != HMMER_MODEL:
					if tacc in accHMMer:
						rankobj[query][i][SORT_SCORE] *= 2
				sortIndexRes[i] = rankobj[query][i][SORT_SCORE]
			for index,value in sorted(sortIndexRes.items(), key=operator.itemgetter(1), reverse=True):
				objReturn[query].append(rankobj[query][index])
		overlappcount = 0
		for query in objReturn:
			for i in range(0,len(objReturn[query])):
				if not objReturn[query][i] is None:
					istart   = objReturn[query][i][SEQ_START]
					iend     = objReturn[query][i][SEQ_END]
					iacc     = objReturn[query][i][TARGET_ACC].split(".")[0]
					iievalue = objReturn[query][i][I_EVALUE]
					iiidentity = objReturn[query][i][SORT_SCORE]
					bestevalue = iievalue;
					bestidentity = iiidentity;
					for j in range(i+1,len(objReturn[query])):
						if not objReturn[query][j] is None:
							jstart   = objReturn[query][j][SEQ_START]
							jend     = objReturn[query][j][SEQ_END]
							jacc     = objReturn[query][j][TARGET_ACC].split(".")[0]
							jievalue = objReturn[query][j][I_EVALUE]
							jiidentity = objReturn[query][j][SORT_SCORE]
							overlapS = FUN.calculateOverlapSize(istart,iend,jstart,jend)
							if overlapS[FUN.OVERLAP_AA] >= FP_OVERLAPPING_THRESHOLD:
								if jiidentity <= bestidentity:
									objReturn[query][j] = None
								else:
									bestidentity = jiidentity
					if iiidentity != bestidentity:
						objReturn[query][i] = None
		for query in objReturn:
			for i in range(0,len(objReturn[query])):
				if not objReturn[query][i] is None:
					data = objReturn[query][i]
					fields = (str(data[I_EVALUE]),data[SCORE],data[MODEL_NAME].split("_")[0],data[MODEL_START],data[MODEL_END],data[TARGET_ACC].split(".")[0],
					data[QUERY_NAME],data[NB_PROB],data[SEQ_START],data[SEQ_END])
					outFile.write("%s\t%f\t%s\t%d\t%d\t%s\t%s\t%.2f\t%d\t%d\n" % fields)
	return objReturn

def main():
	start_time           = time.time()
	parser               = argparse.ArgumentParser(description=DESCRIPTION)
	parser.add_argument(BD_RESULT_DIR_NAME, help=BD_RESULT_DIR_NAME_HELP, type=str)
	parser.add_argument(NB_FILTER_OUT_FILE, help=NB_FILTER_OUT_FILE_HELP, type=str)
	parser.add_argument(ALL_OL_FILTER_OUT_DIR, help=ALL_OL_FILTER_OUT_DIR_HELP, type=str)
	parser.add_argument(FINAL_PREDICTION_OUT_FILE, help=FINAL_PREDICTION_OUT_FILE_HELP, type=str)
	args                 = vars(parser.parse_args())
	bestDomainsPath      = "%s%s" % (args[BD_RESULT_DIR_NAME],args[NB_FILTER_OUT_FILE])
	finalPredictionsPath = "%s%s" % (args[ALL_OL_FILTER_OUT_DIR],args[FINAL_PREDICTION_OUT_FILE])
	if os.path.exists(bestDomainsPath):
		if not os.path.exists(args[ALL_OL_FILTER_OUT_DIR]):
			os.makedirs(args[ALL_OL_FILTER_OUT_DIR])
		filterOverlappingPredictions(bestDomainsPath,finalPredictionsPath)
	else :
		print("Meta CLADE error: Scan File %s Not found" % (bestDomainsPath));
	print("%s\t%f\n"%("Total time",time.time() - start_time));
	return 1

if __name__ == "__main__":
	sys.exit(main())
