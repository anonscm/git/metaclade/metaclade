"""
Author: Ari Ugarte
"""
import sys
import os
from bisect    import bisect_left
from itertools import groupby

#CONSTANTS
OVERLAP_AA     = "overlap_AA"
PERC_OVERLAP   = "perc_overlap"
PERC_OVERLAP_R = "perc_overlap_r"

def calculateOverlapSize(startHit_i,endHit_i,startHit_j,endHit_j):
    overlapAA = 0;
    if (startHit_i <= startHit_j and startHit_i <= endHit_j and endHit_i >= startHit_j and endHit_i >= endHit_j):
        overlapAA = (endHit_j - startHit_j) + 1;
    elif(startHit_i >= startHit_j and startHit_i <= endHit_j and endHit_i >= startHit_j and endHit_i <= endHit_j):
        overlapAA = (endHit_i - startHit_i) + 1;
    elif ( (startHit_i >= startHit_j and startHit_i <= endHit_j)):
        overlapAA = (endHit_j - startHit_i) + 1;
    elif(endHit_i >= startHit_j and endHit_i <= endHit_j):
        overlapAA = (endHit_i - startHit_j) + 1;
    size_i = (endHit_i - startHit_i) + 1;
    size_j = (endHit_j - startHit_j) + 1;
    perc_overlap   = overlapAA/float(size_i);
    perc_overlap_r = overlapAA/float(size_j);
    return {OVERLAP_AA : overlapAA, PERC_OVERLAP : perc_overlap, PERC_OVERLAP_R : perc_overlap_r}

def fasta_iter(fasta_name):
    """
    given a fasta file. yield tuples of header, sequence
    """
    fh = open(fasta_name)
    # ditch the boolean (x[0]) and just keep the header or sequence since
    # we know they alternate.
    faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
    for header in faiter:
        # drop the ">"
        header = header.next()[1:].strip()
        # join all sequence lines to one.
        seq = "".join(s.strip() for s in faiter.next())
        yield header, seq


#CONSTANTS
HMMER_AI_START = 9
HMMER_AI_END   = 10

def hmmer_align_iter(fh):    
    seqiter = ( (key,group) for key,group in groupby(fh, lambda line: line[0:2] == ">>") )
    seqiter.next()
    for key,group in seqiter:
        seq = group.next().strip()[3:]
        key,group = seqiter.next()
        group.next()
        group.next()
        keys = []
        while True:
            line = group.next().strip()
            if line == "":
                break
            elts  = line.split()
            start = int(elts[HMMER_AI_START])
            end   = int(elts[HMMER_AI_END])
            keys.append((start,end))
        domainiter = ((keyi,groupdi) for keyi,groupdi in groupby(group, lambda line: line[2:4] == "=="))
        domainiter.next()
        for keyi,groupdi in domainiter:
            countPos = 0
            keyi,groupdi = domainiter.next()
            for g in groupdi:
                if g.strip() == "":
                    break
                groupdi.next()
                ident = groupdi.next()
                countPos += len(ident.strip().replace(' ','').replace('+',''))
                groupdi.next()
                groupdi.next()
                groupdi.next()
            try:
                start,end = keys.pop(0)
                yield seq,start,end,countPos
            except IndexError:
                pass

def bisect_mclade(iter,value):
    if value == 0:
        return 0
    return bisect_left(iter,value)-1