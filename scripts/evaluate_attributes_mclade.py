"""
Author: Ari Ugarte
"""
import sys
import os
import cPickle
import time
import argparse
import mcladeScriptFunctions as FUN
from itertools import count

#SCRIPT CONSTANTS
MBSCORE_EXT  = ".mbscore"
SCORE_EXT    = ".score"
REGIONS_PROB = ".regionsprob"
HEADER       = "\n\n=== Predictions on test data ===\n\n inst#     actual  predicted error prediction\n"
CLASS_POS    = "POS"

#ARGUMENTS
ARFF_OUT_DIR             = "ARFF_OUT_DIR"
ACC_ID                   = "ACC_ID"
CCM_ARFF_RES_EXT         = "CCM_ARFF_RES_EXT"
SCM_ARFF_RES_EXT         = "SCM_ARFF_RES_EXT"
CCM_EVAL_MODELS_DIR      = "CCM_EVAL_MODELS_DIR"
SCM_EVAL_MODELS_DIR      = "SCM_EVAL_MODELS_DIR"
CCM_EVAL_RES_EXT         = "CCM_EVAL_RES_EXT"
SCM_EVAL_RES_EXT         = "SCM_EVAL_RES_EXT"
EVAL_OUT_DIR             = "EVAL_OUT_DIR"

#MESSAGES
DESCRIPTION              = "This script retrieves the probabilities associated to sequences in an ARFF file"
ARFF_OUT_DIR_HELP        = "Directory for arff files"
ACC_ID_HELP              = "Accesion of the domain"
CCM_ARFF_RES_EXT_HELP    = "CCM arff file extention"
SCM_ARFF_RES_EXT_HELP    = "SCM arff file extention"
CCM_EVAL_MODELS_DIR_HELP = "CCM breaks dir"
SCM_EVAL_MODELS_DIR_HELP = "SCM breaks dir"
CCM_EVAL_RES_EXT_HELP    = "CCM probabilities results extention" 
SCM_EVAL_RES_EXT_HELP    = "SCM probabilities results extention"
EVAL_OUT_DIR_HELP        = "Directory for probabilities results"

def evalDomainResults(acc_id,arff_path,out_path,breaks_dir):
    if os.path.exists(arff_path):
        mbscores_path  = "%s%s%s"    % (breaks_dir,acc_id,MBSCORE_EXT)
        scores_path    = "%s%s%s"    % (breaks_dir,acc_id,SCORE_EXT)
        regsproba_path = "%s%s%s"    % (breaks_dir,acc_id,REGIONS_PROB)
        if os.path.exists(mbscores_path) and os.path.exists(scores_path) and os.path.exists(regsproba_path):
            with open(mbscores_path, "r") as mbscores_file, open(scores_path, "r") as scores_file, open(regsproba_path, "r") as regsproba_file:
                mbscores  = cPickle.load(mbscores_file)
                scores    = cPickle.load(scores_file)
                regsproba = cPickle.load(regsproba_file)
                with open(out_path,"w") as out_file:
                    out_file.write(HEADER)
                    with open(arff_path) as arff_file:
                        for i in range(0,7):
                            arff_file.readline()
                        countline = count(1,1)
                        while True:
                            line = arff_file.readline()
                            if not line:
                                break
                            elts        = line.split(",")
                            score       = float(elts[0])
                            mbscore     = float(elts[1])
                            bis_score   = FUN.bisect_mclade(scores,score)
                            bis_mbscore = FUN.bisect_mclade(mbscores,mbscore)
                            scores_len  = len(scores)
                            line_index  = bis_mbscore * scores_len
                            index_proba = line_index + bis_score
                            try:
                                out_file.write("%d\t1:?\t1:%s\t%f\n" %(countline.next(),CLASS_POS,regsproba[index_proba]))
                            except IndexError:
                                print("MClade Error on %s, index %s not found in %s\n" % (acc_id,index_proba,str(regsproba)))

def main():
    start_time = time.time()
    parser     = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument(ARFF_OUT_DIR,  help=ARFF_OUT_DIR_HELP, type=str)
    parser.add_argument(ACC_ID, help=ACC_ID_HELP, type=str)
    parser.add_argument(CCM_ARFF_RES_EXT, help=CCM_ARFF_RES_EXT_HELP, type=str)
    parser.add_argument(SCM_ARFF_RES_EXT, help=SCM_ARFF_RES_EXT_HELP, type=str)
    parser.add_argument(CCM_EVAL_MODELS_DIR, help=CCM_EVAL_MODELS_DIR_HELP, type=str)
    parser.add_argument(SCM_EVAL_MODELS_DIR, help=SCM_EVAL_MODELS_DIR_HELP, type=str)
    parser.add_argument(CCM_EVAL_RES_EXT, help=CCM_EVAL_RES_EXT_HELP, type=str)
    parser.add_argument(SCM_EVAL_RES_EXT, help=SCM_EVAL_RES_EXT_HELP, type=str)
    parser.add_argument(EVAL_OUT_DIR, help=EVAL_OUT_DIR_HELP, type=str)
    args       = vars(parser.parse_args())
    if not os.path.exists(args[EVAL_OUT_DIR]):
        os.makedirs(args[EVAL_OUT_DIR])
    ccm_arff_path = "%s%s%s"    % (args[ARFF_OUT_DIR],args[ACC_ID],args[CCM_ARFF_RES_EXT])
    scm_arff_path = "%s%s%s"    % (args[ARFF_OUT_DIR],args[ACC_ID],args[SCM_ARFF_RES_EXT])
    ccm_out_path  = "%s%s%s" % (args[EVAL_OUT_DIR],args[ACC_ID],args[CCM_EVAL_RES_EXT])
    scm_out_path  = "%s%s%s" % (args[EVAL_OUT_DIR],args[ACC_ID],args[SCM_EVAL_RES_EXT])
    evalDomainResults(args[ACC_ID],ccm_arff_path,ccm_out_path,args[CCM_EVAL_MODELS_DIR])
    evalDomainResults(args[ACC_ID],scm_arff_path,scm_out_path,args[SCM_EVAL_MODELS_DIR])
    print("%s\t%f\n"%(args[ACC_ID],time.time() - start_time));
    return 1

if __name__ == "__main__":
    sys.exit(main())