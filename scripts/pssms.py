import sys
import os
import tarfile
import shutil
import time
import argparse
import subprocess

#SCRIPT CONSTANTS
PSI_BLAST_PROG               = "psiblast"
PSI_BLAST_OP_DB              = "-db"
PSI_BLAST_OP_IN_PSSM         = "-in_pssm"
PSI_BLAST_OP_OUTFMT          = "-outfmt"
PSI_BLAST_OP_EVALUE          = "-evalue"
PSI_BLAST_OP_OUT             = "-out"
PSI_BLAST_VAL_OUTFMT         = "6"
PSI_BLAST_VAL_EVALUE         = "1"
SUBJECT_INDEX                  = 0
SUBJECT_SEP                  = "_"
SUBJECT_SEP_POS              = "-"
SUBJECT_SPLIT_PROTID         = 0
SUBJECT_SPLIT_TAXON          = 1
SUBJECT_SPLIT_POS            = 2
SUBJECT_SPLIT_POS_START_ORIG = 0
SUBJECT_SPLIT_POS_END_ORIG   = 1
QUERY_INDEX                    = 1
ID_PERC_INDEX                  = 2
AL_LENGTH_INDEX                = 3
MODEL_START_INDEX              = 6
MODEL_END_INDEX                = 7
SEQ_START_INDEX                = 8
SEQ_END_INDEX                  = 9
EVAL_INDEX                     = 10
SCORE_INDEX                    = 11

#ARGUMENTS
ACC_ID                       = "acc_id"
MODEL_ID                     = "model_id"
CCM_DIR                      = "ccm_dir"
PSIBLAST_DIR                 = "psiblast_dir"
FASTA_PATH                   = "fasta_path"
TMP_DIR                      = "tmp_dir"
RESULTS_DIR                  = "results_dir"
DATASET_NAME                 = "dataset_name" 
SEARCH_OUTPUT_DIR_NAME       = "search_output_dir_name"
CCM_ZIP_EXT                  = "ccm_zip_ext"
CCM_EXT                      = "ccm_ext"
CCM_SEARCH_RES_EXT           = "ccm_search_res_ext"

#MESSAGES
DESCRIPTION                  = "This script searches for CCM models matching sequences in the fasta file"
ACC_ID_HELP                  = "Accesion of the domain"
MODEL_ID_HELP                = "CCM id"
CCM_DIR_HELP                 = "Directory of CCM models"
PSIBLAST_DIR_HELP            = "PSI-BLAST Directory"
FASTA_PATH_HELP              = "Fasta File Path"
TMP_DIR_HELP                 = "Directory for temporary files"
RESULTS_DIR_HELP             = "Directory for mclade results"
DATASET_NAME_HELP            = "Dataset Name"
SEARCH_OUTPUT_DIR_NAME_HELP  = "Directory model search step results"
CCM_ZIP_EXT_HELP             = "CCM file compressed extention"
CCM_EXT_HELP                 = "CCM file extention"
CCM_SEARCH_RES_EXT_HELP      = "Model output extention"

def main():
        start_time  = time.time()
	parser      = argparse.ArgumentParser(description=DESCRIPTION)
	parser.add_argument(ACC_ID, help=ACC_ID_HELP, type=str)
	parser.add_argument(MODEL_ID, help=MODEL_ID_HELP, type=str)
	parser.add_argument(CCM_DIR, help=CCM_DIR_HELP, type=str)
	parser.add_argument(PSIBLAST_DIR, help=PSIBLAST_DIR_HELP, type=str)
	parser.add_argument(FASTA_PATH, help=FASTA_PATH_HELP, type=str)
	parser.add_argument(TMP_DIR, help=TMP_DIR_HELP, type=str)
	parser.add_argument(RESULTS_DIR, help=RESULTS_DIR_HELP, type=str)
	parser.add_argument(DATASET_NAME, help=DATASET_NAME, type=str)
	parser.add_argument(SEARCH_OUTPUT_DIR_NAME, help=SEARCH_OUTPUT_DIR_NAME_HELP, type=str)
	parser.add_argument(CCM_ZIP_EXT, help=CCM_ZIP_EXT_HELP, type=str)
	parser.add_argument(CCM_EXT, help=CCM_EXT_HELP, type=str)
	parser.add_argument(CCM_SEARCH_RES_EXT, help=CCM_SEARCH_RES_EXT_HELP, type=str)
	args        = vars(parser.parse_args())
	models_dir  = "%s%s/%s_%s/" % (args[TMP_DIR],args[DATASET_NAME],args[ACC_ID],args[MODEL_ID])
	results_dir = "%s%s/%s/%s/" % (args[RESULTS_DIR],args[DATASET_NAME],args[SEARCH_OUTPUT_DIR_NAME],args[ACC_ID])
	if not os.path.exists(results_dir):
		os.makedirs(results_dir)
	if not os.path.exists(models_dir):
		os.makedirs(models_dir)
	with tarfile.open("%s%s%s" %(args[CCM_DIR], args[ACC_ID], args[CCM_ZIP_EXT])) as model:
		model.extract("%s/%s%s" % (args[ACC_ID], args[MODEL_ID], args[CCM_EXT]), path=models_dir)
	params      = ["%s%s" %(args[PSIBLAST_DIR],PSI_BLAST_PROG),
				PSI_BLAST_OP_DB,args[FASTA_PATH], 
				PSI_BLAST_OP_IN_PSSM,"%s%s/%s%s" % (models_dir,args[ACC_ID],args[MODEL_ID],args[CCM_EXT]), 
				PSI_BLAST_OP_OUTFMT,PSI_BLAST_VAL_OUTFMT,
				PSI_BLAST_OP_EVALUE, PSI_BLAST_VAL_EVALUE,
				PSI_BLAST_OP_OUT, "%s%s/%s%s" % (models_dir,args[ACC_ID],args[MODEL_ID],args[CCM_SEARCH_RES_EXT])]
	psiblast   = subprocess.Popen(params,stdout= subprocess.PIPE)
	psiblast.wait()
	with open("%s%s%s" % (results_dir,args[MODEL_ID],args[CCM_SEARCH_RES_EXT]), "w") as outFile:
		for line in open("%s%s/%s%s" %(models_dir,args[ACC_ID],args[MODEL_ID],args[CCM_SEARCH_RES_EXT])):
			elts              = line.split()
			subject_split     = elts[SUBJECT_INDEX].split(SUBJECT_SEP)
			taxon             = subject_split[SUBJECT_SPLIT_TAXON]
			protId            = str(subject_split[SUBJECT_SPLIT_PROTID])
			subject_split_pos = subject_split[SUBJECT_SPLIT_POS].split(SUBJECT_SEP_POS)
			start_orig        = str(subject_split_pos[SUBJECT_SPLIT_POS_START_ORIG])
			end_orig          = str(subject_split_pos[SUBJECT_SPLIT_POS_END_ORIG])
			seqId             = str(elts[QUERY_INDEX])
			IdPerc            = str(elts[ID_PERC_INDEX])
			AlLength          = str(elts[AL_LENGTH_INDEX])
			model_start       = str(elts[MODEL_START_INDEX])
			model_end         = str(elts[MODEL_END_INDEX])
			seq_start         = str(elts[SEQ_START_INDEX])
			seq_end           = str(elts[SEQ_END_INDEX ])
			eval              = str(elts[EVAL_INDEX])
			score             = str(elts[SCORE_INDEX])
			outFile.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % (protId,start_orig,end_orig,seqId,eval,seq_start,seq_end,score,IdPerc,taxon,model_start,model_end))
		outFile.close()
	shutil.rmtree(models_dir)
	print("%s\t%f\n"%(args[ACC_ID],time.time() - start_time));
	return 1

if __name__ == "__main__":
	sys.exit(main())
