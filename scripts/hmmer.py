import sys
import os
import tarfile
import shutil
import time
import argparse
import subprocess
import mcladeScriptFunctions as FUN

#SCRIPT CONSTANTS
HMMER_PROG           = "hmmsearch"
HMMER_OP_T           = "-T"
HMMER_VAL_T          = "0"
HMMER_OP_OUT         = "-o"
HMMER_OP_DOM_TBL_OUT = "--domtblout"
HMMER_TMP_OUT_EXT    = ".out"
PROTEIN_ID           = 'HMMer-3'
TAXON                = 'ALL'

START_ORIG_INDEX     = 15
END_ORIG_INDEX       = 16
QUERY_INDEX          = 0
ID_PERC_INDEX        = 21
AL_LENGTH_INDEX      = 2
MODEL_START_INDEX    = 15
MODEL_END_INDEX      = 16
SEQ_START_INDEX      = 17
SEQ_END_INDEX        = 18
EVAL_INDEX           = 12
SCORE_INDEX          = 13
QLEN_INDEX           = 5
TLEN_INDEX           = 2

#ARGUMENTS
ACC_ID                       = "acc_id"
SCM_DIR                      = "scm_dir"
HMMER_DIR                    = "hmmer_dir"
FASTA_PATH                   = "fasta_path"
TMP_DIR                      = "tmp_dir"
RESULTS_DIR                  = "results_dir"
DATASET_NAME                 = "dataset_name" 
SEARCH_OUTPUT_DIR_NAME       = "search_output_dir_name"
SCM_EXT                      = "scm_ext"
SCM_SEARCH_RES_EXT           = "scm_search_res_ext"

#MESSAGES
DESCRIPTION                  = "This script searches for SCM models matching sequences in the fasta file"
ACC_ID_HELP                  = "Accesion of the domain"
MODEL_ID_HELP                = "SCM id"
SCM_DIR_HELP                 = "Directory of SCM models"
HMMER_DIR_HELP               = "HMMER Directory"
FASTA_PATH_HELP              = "Fasta File Path"
TMP_DIR_HELP                 = "Directory for temporary files"
RESULTS_DIR_HELP             = "Directory for mclade results"
DATASET_NAME_HELP            = "Dataset Name"
SEARCH_OUTPUT_DIR_NAME_HELP  = "Directory model search step results"
SCM_EXT_HELP                 = "SCM file extention"
SCM_SEARCH_RES_EXT_HELP      = "Model output extention"

def main():
	parser      = argparse.ArgumentParser(description=DESCRIPTION)
	parser.add_argument(ACC_ID, help=ACC_ID_HELP, type=str)
	parser.add_argument(SCM_DIR, help=SCM_DIR_HELP, type=str)
	parser.add_argument(HMMER_DIR, help=HMMER_DIR_HELP, type=str)
	parser.add_argument(FASTA_PATH, help=FASTA_PATH_HELP, type=str)
	parser.add_argument(TMP_DIR, help=TMP_DIR_HELP, type=str)
	parser.add_argument(RESULTS_DIR, help=RESULTS_DIR_HELP, type=str)
	parser.add_argument(DATASET_NAME, help=DATASET_NAME, type=str)
	parser.add_argument(SEARCH_OUTPUT_DIR_NAME, help=SEARCH_OUTPUT_DIR_NAME_HELP, type=str)
	parser.add_argument(SCM_EXT, help=SCM_EXT_HELP, type=str)
	parser.add_argument(SCM_SEARCH_RES_EXT, help=SCM_SEARCH_RES_EXT_HELP, type=str)
	args        = vars(parser.parse_args())
	models_dir  = "%s%s/%s/" % (args[TMP_DIR],args[DATASET_NAME],args[ACC_ID])
	results_dir = "%s%s/%s/%s/" % (args[RESULTS_DIR],args[DATASET_NAME],args[SEARCH_OUTPUT_DIR_NAME],args[ACC_ID])
	if not os.path.exists(results_dir):
		os.makedirs(results_dir)
	if not os.path.exists(models_dir):
		os.makedirs(models_dir)
	shutil.copy("%s%s%s" %(args[SCM_DIR],args[ACC_ID],args[SCM_EXT]), models_dir)
	start_time  = time.time()
	params      = ["%s%s" %(args[HMMER_DIR],HMMER_PROG),
				HMMER_OP_T , HMMER_VAL_T,
				HMMER_OP_OUT , "%s%s%s" %(models_dir,args[ACC_ID],HMMER_TMP_OUT_EXT),
				HMMER_OP_DOM_TBL_OUT, "%s%s%s" %(models_dir,args[ACC_ID],args[SCM_SEARCH_RES_EXT]),
				"%s%s%s" %(models_dir,args[ACC_ID],args[SCM_EXT]), args[FASTA_PATH]]
	hmmsearch   = subprocess.Popen(params, stdout= subprocess.PIPE)
	hmmsearch.wait()
	percentIden = {}
	with open("%s%s%s" % (models_dir, args[ACC_ID], HMMER_TMP_OUT_EXT), "r") as resAlignFile:
		iterHMMRes = FUN.hmmer_align_iter(resAlignFile)
		for seq,start,end,countPos in iterHMMRes:
			percentIden["%s_%d_%d" % (seq,start,end)] = countPos
	with open("%s%s%s" % (results_dir,args[ACC_ID],args[SCM_SEARCH_RES_EXT]), "w") as outFile, open("%s%s%s" % (models_dir, args[ACC_ID], args[SCM_SEARCH_RES_EXT]), "r") as resFile:
		resFile.readline()
		resFile.readline()
		resFile.readline()
		for line in resFile:
			if not line[0] == '#':				
				elts        = line.split()
				start_orig  = str(elts[START_ORIG_INDEX])
				end_orig    = str(elts[END_ORIG_INDEX])
				seqId       = str(elts[QUERY_INDEX])
				AlLength    = str(elts[AL_LENGTH_INDEX])
				model_start = str(elts[MODEL_START_INDEX])
				model_end   = str(elts[MODEL_END_INDEX])
				seq_start   = str(elts[SEQ_START_INDEX])
				seq_end     = str(elts[SEQ_END_INDEX])
				eval        = str(elts[EVAL_INDEX])
				score       = str(elts[SCORE_INDEX])
				try:
					countPos = percentIden["%s_%s_%s" % (seqId,seq_start,seq_end)]
					percId   = float(countPos) /min(float(elts[QLEN_INDEX]),float(elts[TLEN_INDEX ]))
				except (KeyError,IndexError) as e:
					percId   = 0.0
				outFile.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%.2f\t%s\t%s\t%s\n" % (PROTEIN_ID,start_orig,end_orig,seqId,eval,seq_start,seq_end,score,percId,TAXON,model_start,model_end))
	shutil.rmtree(models_dir)
	print("%s\t%f\n"%(args[ACC_ID],time.time() - start_time));
	return 1

if __name__ == "__main__":
	sys.exit(main())