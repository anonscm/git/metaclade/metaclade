"""
Author: Ari Ugarte
"""
import sys
import os
import ConfigParser
import subprocess
import argparse
import collections
import cPickle
import mcladeConstants as C
		
class MetaCLADEController:
	def __init__(self,ini,run_ini):
		self.ini            = ini
		self.run_ini        = run_ini
		if not os.path.exists(self.ini):
			raise IOError(C.INPUT_ERROR_MCLADE_CONFIG)
		if not os.path.exists(self.run_ini):
			raise IOError(C.INPUT_ERROR_DATASET_CONFIG)
		self.MetaConfig     = ConfigParser.ConfigParser()
		self.RunConfig      = ConfigParser.ConfigParser()
		self.MetaConfigDef  = collections.OrderedDict()
		self.RunConfigDef   = collections.OrderedDict()
	
	def createFiles(self):
		self.parseConfig().createDirectories()
		if self.CREATE_BLASTDB:
			self.createBlastDB()
		if self.SEARCH:
			self.createSearchFiles()
		if self.SAME_OL_FILTER:
			self.createOverLappingFilterFiles()
		if self.NB_FILTER:
			self.createNaiveBayesEvalFiles()
			self.createNaiveBayesFilterFile()
		if self.ALL_OL_FILTER:
			self.createAllNonOverlappingFile()
		return self
		
	def setMetaCLADEDefaults(self):
		for not_in_config_section in (section for section in C.METACLADE_CONFIG_SECTIONS if not self.MetaConfig.has_section(section)):
			self.MetaConfig.add_section(not_in_config_section)
		self.MetaConfigDef[C.RUNENV_TMP_DIR.lower()]        = C.RUNENV_TMP_DIR_DEFAULT        % (self.BASE_DIR)
		self.MetaConfigDef[C.RUNENV_RESULTS_DIR.lower()]    = C.RUNENV_RESULTS_DIR_DEFAULT    % (self.BASE_DIR)
		self.MetaConfigDef[C.RUNENV_JOBS_DIR.lower()]       = C.RUNENV_JOBS_DIR_DEFAULT       % (self.BASE_DIR)
		self.MetaConfigDef[C.RUNENV_SCRIPTS_DIR.lower()]    = C.RUNENV_SCRIPTS_DIR_DEFAULT    % (self.BASE_DIR)
		self.MetaConfigDef[C.RUNENV_FILES_DIR.lower()]      = C.RUNENV_FILES_DIR_DEFAULT      % (self.BASE_DIR)
		self.MetaConfigDef[C.RUNENV_MODEL_LIST.lower()]     = C.RUNENV_MODEL_LIST_DEFAULT     % (self.BASE_DIR, "%s")
		self.MetaConfigDef[C.RUNENV_PYTHON_PATH.lower()]    = C.RUNENV_PYTHON_PATH_DEFAULT
		self.MetaConfigDef[C.DOMAINS_LIST.lower()]          = C.DOMAINS_LIST_DEFAULT          % (self.BASE_DIR)
		self.MetaConfigDef[C.MODELS_PSSMS_DIR.lower()]      = C.MODELS_PSSMS_DIR_DEFAULT      % (self.BASE_DIR)
		self.MetaConfigDef[C.MODELS_HMMS_DIR.lower()]       = C.MODELS_HMMS_DIR_DEFAULT       % (self.BASE_DIR)
		self.MetaConfigDef[C.MODELS_CCM_BREAKS_DIR.lower()] = C.MODELS_CCM_BREAKS_DIR_DEFAULT % (self.BASE_DIR)
		self.MetaConfigDef[C.MODELS_SCM_BREAKS_DIR.lower()] = C.MODELS_SCM_BREAKS_DIR_DEFAULT % (self.BASE_DIR)
		return self
		
	def getMetaCLADEDefaults(self):
		self.setMetaCLADEDefaults()
		self.MetaConfig._defaults = self.MetaConfigDef
		return self
	
	def setDatasetDefaults(self):
		self.RunConfigDef[C.PARAMETERS_NUMBER_OF_JOBS.lower()] = C.PARAMETERS_NUMBER_OF_JOBS_DEFAULT
		self.RunConfigDef[C.PARAMETERS_CREATE_BLASTDB.lower()] = C.PARAMETERS_CREATE_BLASTDB_DEFAULT
		self.RunConfigDef[C.PARAMETERS_SEARCH.lower()]         = C.PARAMETERS_SEARCH_DEFAULT
		self.RunConfigDef[C.PARAMETERS_SAME_OL_FILTER.lower()] = C.PARAMETERS_SAME_OL_FILTER_DEFAULT
		self.RunConfigDef[C.PARAMETERS_NB_FILTER.lower()]      = C.PARAMETERS_NB_FILTER_DEFAULT
		self.RunConfigDef[C.PARAMETERS_ALL_OL_FILTER.lower()]  = C.PARAMETERS_ALL_OL_FILTER_DEFAULT
		self.RunConfigDef[C.PARAMETERS_NB_THRESHOLD.lower()]   = C.PARAMETERS_NB_THRESHOLD_DEFAULT
		return self
	
	def getDatasetDefaults(self):
		self.setDatasetDefaults()
		self.RunConfig._defaults = self.RunConfigDef
		return self
	
	def parseConfig(self):
		#Read MetaCLADE config file
		self.MetaConfig.read(self.ini)
		#RunEnv BaseDir
		self.BASE_DIR            = self.MetaConfig.get(C.RUNENV_SECTION, C.RUNENV_BASE_DIR)
		self.getMetaCLADEDefaults()
		self.TMP                 = self.MetaConfig.get(C.RUNENV_SECTION, C.RUNENV_TMP_DIR)
		self.MODELS_LIST         = self.MetaConfig.get(C.RUNENV_SECTION, C.RUNENV_MODEL_LIST)
		self.RESULTS_DIR         = self.MetaConfig.get(C.RUNENV_SECTION, C.RUNENV_RESULTS_DIR)
		self.JOBS_DIR            = self.MetaConfig.get(C.RUNENV_SECTION, C.RUNENV_JOBS_DIR)
		self.SCRIPTS_DIR         = self.MetaConfig.get(C.RUNENV_SECTION, C.RUNENV_SCRIPTS_DIR)
		self.FILES_DIR           = self.MetaConfig.get(C.RUNENV_SECTION, C.RUNENV_FILES_DIR)
		self.PYTHON_PATH         = self.MetaConfig.get(C.RUNENV_SECTION, C.RUNENV_PYTHON_PATH)
		#ProgramPaths
		self.PSIBLAST_PATH       = self.MetaConfig.get(C.PROGRAM_SECTION, C.PROGRAM_PATHS_PSIBLAST_DIR)
		self.HMMER_PATH          = self.MetaConfig.get(C.PROGRAM_SECTION, C.PROGRAM_PATHS_HMMER_DIR)
		#Models
		self.PSSMS_PATH          = self.MetaConfig.get(C.MODEL_SECTION, C.MODELS_PSSMS_DIR)
		self.HMMS_PATH           = self.MetaConfig.get(C.MODEL_SECTION, C.MODELS_HMMS_DIR)
		self.CCM_EVAL_MODELS_DIR = self.MetaConfig.get(C.MODEL_SECTION, C.MODELS_CCM_BREAKS_DIR)
		self.SCM_EVAL_MODELS_DIR = self.MetaConfig.get(C.MODEL_SECTION, C.MODELS_SCM_BREAKS_DIR)
		#Acc List
		self.ACC_LIST_PATH       = self.MetaConfig.get(C.DOMAINS_SECTION, C.DOMAINS_LIST)
		
		#Read Dataset config file
		self.RunConfig.read(self.run_ini)
		self.getDatasetDefaults()
		#Parameters
		self.FASTA_PATH     = self.RunConfig.get(C.PARAMETERS_SECTION,C.PARAMETERS_FASTA_FILE)
		self.DATASET_NAME   = self.RunConfig.get(C.PARAMETERS_SECTION,C.PARAMETERS_DATASET_NAME)
		self.NUMBER_OF_JOBS = int(self.RunConfig.get(C.PARAMETERS_SECTION,C.PARAMETERS_NUMBER_OF_JOBS))
		self.NB_THRESHOLD   = float(self.RunConfig.get(C.PARAMETERS_SECTION,C.PARAMETERS_NB_THRESHOLD))
		self.CREATE_BLASTDB = self.str_to_bool(self.RunConfig.get(C.PARAMETERS_SECTION,C.PARAMETERS_CREATE_BLASTDB))
		self.SEARCH         = self.str_to_bool(self.RunConfig.get(C.PARAMETERS_SECTION,C.PARAMETERS_SEARCH))
		self.SAME_OL_FILTER = self.str_to_bool(self.RunConfig.get(C.PARAMETERS_SECTION,C.PARAMETERS_SAME_OL_FILTER))
		self.NB_FILTER      = self.str_to_bool(self.RunConfig.get(C.PARAMETERS_SECTION,C.PARAMETERS_NB_FILTER))
		self.ALL_OL_FILTER  = self.str_to_bool(self.RunConfig.get(C.PARAMETERS_SECTION,C.PARAMETERS_ALL_OL_FILTER))
		return self;
	
	def createDirectories(self):
		if not os.path.exists(self.RESULTS_DIR):
			os.makedirs(self.RESULTS_DIR)
		if not os.path.exists(self.JOBS_DIR):
			os.makedirs(self.JOBS_DIR)
		if not os.path.exists(self.FILES_DIR):
			os.makedirs(self.FILES_DIR)
		return self
	
	def str_to_bool(self,s):
		if s.lower() == C.STR_TRUE:
			return True
		elif s.lower() == C.STR_FALSE:
			return False
		else:
			raise ValueError
	
	def createBlastDB(self):
		makeblasDBParams   = ["%s%s" % (self.PSIBLAST_PATH,C.BLAST_MAKEBLASTDB),C.BLAST_IN,self.FASTA_PATH,C.BLAST_DBTYPE,C.BLAST_PROT]
		makeblastDB        = subprocess.Popen(makeblasDBParams, stdout= subprocess.PIPE)
		makeblastDB.wait()
		return self;
	
	def createQsubJob(self,jobDir,jobOutDir):
		outPathC  = "%s%s%s" % (jobDir, C.QSUB_SCRIPT_NAME, C.SCRIPT_EXT)
		with open(outPathC,"w") as outFileC:
			for i in range(0,self.NUMBER_OF_JOBS):
				outFileC.write(C.QSUB_LINE % (self.DATASET_NAME,i,jobOutDir,jobOutDir,"%s%s" % (jobDir, self.DATASET_NAME),i,C.SCRIPT_EXT))
			outFileC.close()
		return self;
	
	def createSearchFiles(self):
		jobDir    = "%s/%s/%s/" % (self.JOBS_DIR, self.DATASET_NAME, C.SEARCH_DIR_NAME)
		jobOutDir = "%s/%s"    % (jobDir, C.OUT_DIR_NAME)
		if not os.path.exists(jobOutDir):
			os.makedirs(jobOutDir)
		return self.getModelsList().createSearchJobs(jobDir).createQsubJob(jobDir,jobOutDir)
		
	def searchHMM(self,pfam,fastaPath):
		return " ".join((self.PYTHON_PATH, "%s%s" %(self.SCRIPTS_DIR , C.HMMER_SCRIPT), pfam, self.HMMS_PATH, self.HMMER_PATH, fastaPath ,self.TMP, self.RESULTS_DIR, 
		self.DATASET_NAME,C.SEARCH_OUTPUT_DIR_NAME,C.SCM_EXT,C.SCM_SEARCH_RES_EXT,"\n"))
	
	def searchPSI(self,pfam,model,fastaPath):
		return " ".join((self.PYTHON_PATH, "%s%s" %(self.SCRIPTS_DIR , C.PSSMS_SCRIPT), pfam, model, self.PSSMS_PATH, self.PSIBLAST_PATH, fastaPath , self.TMP, self.RESULTS_DIR, 
		self.DATASET_NAME,C.SEARCH_OUTPUT_DIR_NAME,C.CCM_ZIP_EXT,C.CCM_EXT,C.CCM_SEARCH_RES_EXT,"\n"))
		
	def getModelsList(self):
		try:
			with open("%s%s" % (self.FILES_DIR, C.PSSMS_MODELS_DICT), "r") as pssms_models_dict_file:		
				self.domModels = cPickle.load(pssms_models_dict_file)
		except IOError:
			return self.setModelsList().getModelsList()
		return self;
	
	def setModelsList(self):
		self.domModels   = {}
		for line in open(self.ACC_LIST_PATH):
			pfam = line.split()[0]
			self.domModels[pfam] = []
			pathList = self.MODELS_LIST % pfam
			if os.path.exists(pathList):
				for line in open(pathList):
					elts      = line.split()
					modelName = "".join((elts[1],"_",elts[2],"_",elts[3]))
					self.domModels[pfam].append(modelName)
		with open("%s%s" % (self.FILES_DIR, C.PSSMS_MODELS_DICT), "w") as pssms_models_dict_file:
			cPickle.dump(self.domModels, pssms_models_dict_file)
		return self;
	
	def createSearchJobs(self,jobDir):
		fileArray = [open("%s%s_%d%s" %(jobDir,self.DATASET_NAME,i,C.SCRIPT_EXT),"w") for i in range(0,self.NUMBER_OF_JOBS)];
		index     = 0
		for line in open(self.ACC_LIST_PATH):
			pfam   = line.split()[0]	
			fileArray[index].write(self.searchHMM(pfam,self.FASTA_PATH))
			for model in self.domModels[pfam]:
				fileArray[index].write(self.searchPSI(pfam,model,self.FASTA_PATH))
			index += 1
			if index == self.NUMBER_OF_JOBS:
				index = 0
		for f in fileArray:
			f.close()
		return self;
	
	def createOverLappingFilterJobs(self,jobDir):
		searchResultsDir = "%s%s/%s/" % (self.RESULTS_DIR,self.DATASET_NAME,C.SEARCH_OUTPUT_DIR_NAME)
		arffFilesDir     = "%s%s/%s/" % (self.RESULTS_DIR,self.DATASET_NAME,C.ARFF_RESULT_DIR_NAME)
		fileArray        = [open("%s%s_%d%s" %(jobDir,self.DATASET_NAME,i,C.SCRIPT_EXT),"w") for i in range(0,self.NUMBER_OF_JOBS)];
		index            = 0
		for line in open(self.ACC_LIST_PATH):
			accId   = line.split()[0]
			params  = (self.PYTHON_PATH, "%s%s" % (self.SCRIPTS_DIR,C.OL_FILTER_SCRIPT),searchResultsDir,accId,arffFilesDir,C.CCM_SEARCH_RES_EXT,
			C.SCM_SEARCH_RES_EXT,C.CCM_ARFF_RES_EXT,C.SCM_ARFF_RES_EXT,C.CCM_RESULT_RESUME,C.SCM_RESULT_RESUME)
			command = "%s %s %s %s %s %s %s %s %s %s %s\n" % params
			fileArray[index].write(command)
			index  += 1
			if index == self.NUMBER_OF_JOBS:
				index = 0
		for f in fileArray:
			f.close()
		return self;
	
	def createOverLappingFilterFiles(self):
		jobDir    = "%s/%s/%s/" % (self.JOBS_DIR,self.DATASET_NAME,C.ARFF_JOB_DIR_NAME)
		jobOutDir = "%s/%s"     % (jobDir, C.OUT_DIR_NAME)
		if not os.path.exists(jobOutDir):
			os.makedirs(jobOutDir)
		return self.createOverLappingFilterJobs(jobDir).createQsubJob(jobDir,jobOutDir)
	
	def createNaiveBayesEvalJobs(self,jobDir):
		arffFilesDir     = "%s%s/%s/" % (self.RESULTS_DIR,self.DATASET_NAME,C.ARFF_RESULT_DIR_NAME)
		probaFilesDir    = "%s%s/%s/" % (self.RESULTS_DIR,self.DATASET_NAME,C.NB_RESULT_DIR_NAME)
		fileArray        = [open("%s%s_%d%s" %(jobDir,self.DATASET_NAME,i,C.SCRIPT_EXT),"w") for i in range(0,self.NUMBER_OF_JOBS)];
		index            = 0
		for line in open(self.ACC_LIST_PATH):
			pfam    = line.split()[0]
			params  = (self.PYTHON_PATH,"%s%s" % (self.SCRIPTS_DIR, C.NB_EVAL_SCRIPT), arffFilesDir,pfam,C.CCM_ARFF_RES_EXT,C.SCM_ARFF_RES_EXT,self.CCM_EVAL_MODELS_DIR,self.SCM_EVAL_MODELS_DIR,
			C.CCM_EVAL_RES_EXT,C.SCM_EVAL_RES_EXT,probaFilesDir)
			command = "%s %s %s %s %s %s %s %s %s %s %s\n" % params
			fileArray[index].write(command)
			index  += 1
			if index == self.NUMBER_OF_JOBS:
				index = 0
		for f in fileArray:
			f.close()
		return self;
	
	def createNaiveBayesEvalFiles(self):
		jobDir    = "%s/%s/%s/" % (self.JOBS_DIR, self.DATASET_NAME, C.NB_JOB_DIR_NAME)
		jobOutDir = "%s/%s"     % (jobDir, C.OUT_DIR_NAME)
		if not os.path.exists(jobOutDir):
			os.makedirs(jobOutDir)
		return self.createNaiveBayesEvalJobs(jobDir).createQsubJob(jobDir,jobOutDir)
	
	def createNaiveBayesFilterJob(self,jobDir):
		searchResultsDir   = "%s%s/%s/" % (self.RESULTS_DIR,self.DATASET_NAME,C.SEARCH_OUTPUT_DIR_NAME)
		bestdomainsFileDir = "%s%s/%s/" % (self.RESULTS_DIR,self.DATASET_NAME,C.BD_RESULT_DIR_NAME)
		evalFileDir        = "%s%s/%s/" % (self.RESULTS_DIR,self.DATASET_NAME,C.NB_RESULT_DIR_NAME)
		params           = (self.PYTHON_PATH,"%s%s" % (self.SCRIPTS_DIR, C.NB_FILTER_SCRIPT),self.ACC_LIST_PATH, evalFileDir, C.CCM_EVAL_RES_EXT, C.SCM_EVAL_RES_EXT, searchResultsDir,
		C.CCM_RESULT_RESUME, C.SCM_RESULT_RESUME, bestdomainsFileDir, C.NB_FILTER_OUT_FILE, self.NB_THRESHOLD,"%s%s" % (self.FILES_DIR, C.MIN_NEG_CCM), "%s%s" % (self.FILES_DIR, C.MIN_NEG_SCM),
		"%s%s" % (self.FILES_DIR, C.MIN_POS_CCM), "%s%s" % (self.FILES_DIR,C.MIN_POS_SCM) )
		with open("%s%s%s" % (jobDir,self.DATASET_NAME,C.SCRIPT_EXT),"w") as outFile:
			outFile.write("%s %s %s %s %s %s %s %s %s %s %s %f %s %s %s %s\n" %(params))
		return self

	def createNaiveBayesFilterFile(self):
		jobDir    = "%s/%s/%s/" % (self.JOBS_DIR, self.DATASET_NAME, C.BD_JOB_DIR_NAME)
		jobOutDir = "%s/%s"     % (jobDir, C.OUT_DIR_NAME)
		if not os.path.exists(jobOutDir):
			os.makedirs(jobOutDir)
		return self.createNaiveBayesFilterJob(jobDir)
	
	def createAllNonOverlappingJob(self,jobDir):
		finalpredictionsDir = "%s%s/%s/" % (self.RESULTS_DIR,self.DATASET_NAME,C.FP_RESULT_DIR_NAME)
		bestdomainsDir      = "%s%s/%s/" % (self.RESULTS_DIR,self.DATASET_NAME,C.BD_RESULT_DIR_NAME)
		params = (self.PYTHON_PATH,"%s%s" % (self.SCRIPTS_DIR, C.ALL_OV_SCRIPT),bestdomainsDir,C.NB_FILTER_OUT_FILE,finalpredictionsDir,C.FINAL_PREDICTION_OUT_FILE)
		with open("%s%s%s" % (jobDir,self.DATASET_NAME,C.SCRIPT_EXT),"w") as outFile:
			outFile.write("%s %s %s %s %s %s\n" % (params))
		return self

	def createAllNonOverlappingFile(self):
		jobDir    = "%s/%s/%s/" % (self.JOBS_DIR, self.DATASET_NAME, C.FP_JOB_DIR_NAME)
		jobOutDir = "%s/%s"     % (jobDir, C.OUT_DIR_NAME)
		if not os.path.exists(jobOutDir):
			os.makedirs(jobOutDir)
		return self.createAllNonOverlappingJob(jobDir)
	
def main():
	parser = argparse.ArgumentParser(description=C.CONTROLLER_DESCRIPTION)
	parser.add_argument(C.MC_INI,  help=C.MC_INI_HELP, type=str)
	parser.add_argument(C.RUN_INI, help=C.RUN_INI_HELP, type=str)
	args   = parser.parse_args()
	try:
		MetaCLADE = MetaCLADEController(args.MC_ini,args.Run_ini)
		MetaCLADE.createFiles()
	except (IOError,ConfigParser.NoOptionError) as error:
		print(C.CONTROLLER_ERROR % (error))
	return 1;

if __name__ == "__main__":
	sys.exit(main())